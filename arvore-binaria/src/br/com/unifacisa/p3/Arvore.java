package br.com.unifacisa.p3;

import java.util.Stack;

/**
 * Exercicio - 11 disciplina de p3;
 * 
 * 
 * Questao 7:
 * 
 * Arvore com percurso pos-ordem: FCBDG - Sua raiz � a G
 * Arvore com percurso pre-ordem: IBCDFEN - Sua raiz � a I
 * Arvore com percurso in-ordem: CBIDFGE - Sua raiz � a D
 * @author Pedro
 *
 */
public class Arvore {

	private No raiz;

	private String aux;

	/**
	 * Inserir algum valor na arvore binaria
	 * 
	 * @param valor
	 */
	public void inserir(int valor) {
		inserir(this.raiz, valor);
	}

	/**
	 * Metodo recursivo para adicionar o valor
	 * 
	 * @param raiz
	 * @param valor
	 */
	private void inserir(No raiz, int valor) {
		if (this.raiz == null) {
			this.raiz = new No(valor);
		} else {
			if (valor <= raiz.getValor()) {
				if (raiz.getEsquerdo() == null) {
					raiz.setEsquerdo(new No(valor));
				} else {
					inserir(raiz.getEsquerdo(), valor);
				}
			} else {
				if (raiz.getDireito() == null) {
					raiz.setDireito(new No(valor));
				} else {
					inserir(raiz.getDireito(), valor);
				}
			}
		}

	}

	/**
	 * Metodo para percorrer todos os valores da arvore
	 */
	public String percorrer() {
		this.aux = "";
		percorrer(this.raiz);
		return this.aux.trim();
	}

	/**
	 * Metodo recursivo para percorrer todos os valores da arvore
	 * 
	 * @param raiz
	 */
	private void percorrer(No raiz) { // implementacao de preOrdem para verificar os valores inseridos na arvore
		if (raiz != null) {
				aux += raiz.getValor() + " ";
			if (raiz.getEsquerdo() != null) {
				percorrer(raiz.getEsquerdo());
			}
			if (raiz.getDireito() != null) {
				percorrer(raiz.getDireito());
			}
		}
	}

	/**
	 * Metodo para obter a quantidade de no em uma arvore
	 */
	public int getQtdNo() {
		return getQtdNo(this.raiz);
	}

	/**
	 * Metodo recursivo para retornar a quantidade de folha s em uma arvore
	 */
	private int getQtdNo(No raiz) {
		int cont = 0;
		if (raiz != null) {
			cont += 1;
			if (raiz.getEsquerdo() != null) {
				cont += getQtdNo(raiz.getEsquerdo());
			}
			if (raiz.getDireito() != null) {
				cont += getQtdNo(raiz.getDireito());
			}

		}
		return cont;
	}

	/**
	 * Metodo para obter a quantidade de no em uma arvore
	 */
	public int getNumeroFolha() {
		return getNumeroFolha(this.raiz);
	}

	/**
	 * Metodo recursivo pra verificar a quantidade de folhas
	 * 
	 * @param raiz
	 */
	private int getNumeroFolha(No raiz) {
		int cont = 0;
		if (raiz != null) {
			if (raiz.getDireito() == null && raiz.getEsquerdo() == null) {
				return 1;
			}
			cont += getNumeroFolha(raiz.getEsquerdo());
			cont += getNumeroFolha(raiz.getDireito());
		}
		return cont;
	}

	/**
	 * Metodo para verificar a altura de uma arvore
	 */
	public int getAltura() {
		return getAltura(this.raiz);
	}

	/**
	 * Metodo recursivo para verificar a altura da arvore
	 * 
	 * @param raiz
	 */
	private int getAltura(No raiz) {
		if (raiz == null) {
			return -1;
		} else {

			int alturaEsquerda = getAltura(raiz.getEsquerdo());
			int alturaDireita = getAltura(raiz.getDireito());

			if (alturaEsquerda > alturaDireita) {
				return alturaEsquerda + 1;
			} else {
				return alturaDireita + 1;
			}
		}

	}

	/**
	 * Metodo para verificar se a arvore esta vazia
	 */
	public boolean isEmpty() {
		return this.raiz == null;
	}

	/**
	 * Metodo para verificar se a arvore � cheia
	 */
	public boolean isFull() {
		return Math.pow(2, getAltura()) == getNumeroFolha();
	}

	/**
	 * Metodo preOrdem interativo usando pilha
	 */
	public String preOrdem() { // Implementacao interativa preOrdem com pilha
		StringBuilder palavra = new StringBuilder();
		if (this.raiz != null) {
			Stack<No> pilha = new Stack<No>();
			pilha.push(this.raiz);
			while (!pilha.isEmpty()) {
				No atual = pilha.pop();
				palavra.append(atual.getValor() + " ");
				if (atual.getDireito() != null) {
					pilha.push(atual.getDireito());
				}
				if (atual.getEsquerdo() != null) {
					pilha.push(atual.getEsquerdo());
				}
			}

		}
		return palavra.toString().trim();
	}

	/**
	 * Metodo inOrdem interativo usando pilha
	 */
	public String inOrdem() { // implementacao interativo inOrdem com pilha
		StringBuilder palavra = new StringBuilder();
		No raiz = this.raiz;
		Stack<No> pilha = new Stack<No>();
		while (raiz != null || !pilha.isEmpty()) {
			if (raiz != null) {
				pilha.push(raiz);
				raiz = raiz.getEsquerdo();
			} else {
				raiz = pilha.pop();
				palavra.append(raiz.getValor() + " ");
				raiz = raiz.getDireito();
			}
		}
		return palavra.toString().trim();
	}

}
