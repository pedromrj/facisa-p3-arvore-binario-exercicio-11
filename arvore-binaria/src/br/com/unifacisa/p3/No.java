package br.com.unifacisa.p3;

public class No {
	
	private No direito;
	
	private No esquerdo;
	
	private int valor;

	public No(int valor) {
		this.valor = valor;
	}

	/**
	 * Metodo usado para retornar o atributo
	 * @return o atributo direito
	 */
	public No getDireito() {
		return direito;
	}

	/**
	 * Metodo usado para ajustar o objeto da classe
	 * @param direito ajusta o atributo direito
	 */
	public void setDireito(No direito) {
		this.direito = direito;
	}

	/**
	 * Metodo usado para retornar o atributo
	 * @return o atributo esquerdo
	 */
	public No getEsquerdo() {
		return esquerdo;
	}

	/**
	 * Metodo usado para ajustar o objeto da classe
	 * @param esquerdo ajusta o atributo esquerdo
	 */
	public void setEsquerdo(No esquerdo) {
		this.esquerdo = esquerdo;
	}

	/**
	 * Metodo usado para retornar o atributo
	 * @return o atributo valor
	 */
	public int getValor() {
		return valor;
	}

	/**
	 * Metodo usado para ajustar o objeto da classe
	 * @param valor ajusta o atributo valor
	 */
	public void setValor(int valor) {
		this.valor = valor;
	}
	
	
}
