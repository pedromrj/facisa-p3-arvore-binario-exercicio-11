package br.com.unifacisa.p3.teste;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.unifacisa.p3.Arvore;

/**
 * Testando implemetacao Arvore do exercicio 11
 * 
 * @author Pedro
 *
 */
class ArvoreTeste {

	/**
	 * Objeto de teste
	 */
	Arvore arvore = new Arvore();

	/**
	 * Metodo para resetar os objetos
	 * 
	 * @throws Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		this.arvore = new Arvore();
	}

	/**
	 * Testando metodo inserir, para adicionar valor a arvore
	 */
	@Test
	void inserirTeste1() {

		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(5);

		Assertions.assertEquals(5, arvore.getQtdNo());

	}

	/**
	 * Testando metodo inserir, para adicionar valor a arvore degenerada e verificar
	 * os valores
	 */
	@Test
	void inserirTeste2() {

		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(5);

		Assertions.assertEquals("1 2 3 4 5", arvore.percorrer());

	}

	/**
	 * Testando metodo inserir, para adicionar valor a arvore e verificar os valores
	 */
	@Test
	void inserirTeste3() {

		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);

		Assertions.assertEquals("8 3 1 6 4 7 10 14 13", arvore.percorrer());

	}

	/**
	 * Testando metodo inserir, adicionando uma arvore cheia
	 */
	@Test
	void inserirTeste4() {
		arvore.inserir(3);
		arvore.inserir(2);
		arvore.inserir(5);
		arvore.inserir(1);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(6);

		Assertions.assertEquals("3 2 1 3 5 4 6", arvore.percorrer());

	}

	/**
	 * Testando metodo getNumeroFolha, para retornar o valor da quantidade de folhas
	 */
	@Test
	void getNumeroFolhaTeste1() {
		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(5);
		Assertions.assertEquals(1, arvore.getNumeroFolha());
	}

	/**
	 * Testando metodo getNumeroFolha, para retornar o valor da quantidade de
	 * folhas, em uma arvore vazia
	 */
	@Test
	void getNumeroFolhaTeste2() {

		Assertions.assertEquals(0, arvore.getNumeroFolha());
	}

	/**
	 * Testando metodo getNumeroFolha, para retornar o valor da quantidade de folhas
	 * de uma arvore nao degenerada;
	 */
	@Test
	void getNumeroFolhaTeste3() {
		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);

		Assertions.assertEquals(4, arvore.getNumeroFolha());
	}
	
	/**
	 * Testando metodo getNumeroFolha em uma arvore cheia
	 */
	@Test
	void getNumeroFolhaTeste4() {
		arvore.inserir(3);
		arvore.inserir(2);
		arvore.inserir(5);
		arvore.inserir(1);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(6);
		Assertions.assertEquals(4, arvore.getNumeroFolha());
	}

	/**
	 * Testando metodo getAltura, para retornar a altura da arvore
	 */
	@Test
	void getAlturaTeste1() {
		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);

		Assertions.assertEquals(3, arvore.getAltura());
	}

	/**
	 * Testando metodo getAltura, para retornar a altura da arvore
	 */
	@Test
	void getAlturaTeste2() {
		arvore.inserir(8);

		Assertions.assertEquals(0, arvore.getAltura());
	}

	/**
	 * Testando metodo getAltura, para retornar a altura da arvore vazia
	 */
	@Test
	void getAlturaTeste3() {
		Assertions.assertEquals(-1, arvore.getAltura());
	}

	/**
	 * Testando metodo getAltura, para retornar a altura de uma arvore degenerada
	 * 
	 */
	@Test
	void getAlturaTeste4() {
		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		Assertions.assertEquals(3, arvore.getAltura());

	}

	/**
	 * Testando metodo getAltura em uma arvore cheia
	 */
	@Test
	void getAlturaTeste5() {
		arvore.inserir(3);
		arvore.inserir(2);
		arvore.inserir(5);
		arvore.inserir(1);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(6);
		Assertions.assertEquals(2, arvore.getAltura());
	}

	/**
	 * Testando metodo getQtdNo, para retornar a quantidade de No de uma arvore
	 * degenerada
	 * 
	 */
	@Test
	void getQtdNoTeste1() {
		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		Assertions.assertEquals(4, arvore.getQtdNo());

	}

	/**
	 * Testando metodo getQtdNo, para retornar a quantidade de No de uma arvore
	 * vazia
	 */
	@Test
	void getQtdNoTeste2() {

		Assertions.assertEquals(0, arvore.getQtdNo());

	}

	/**
	 * Testando metodo getQtdNo, para retornar a quantidade de No de uma arvore
	 */
	@Test
	void getQtdNoTeste3() {
		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);
		Assertions.assertEquals(9, arvore.getQtdNo());

	}

	/**
	 * Testando metodo getQtdNo em uma arvore cheia
	 */
	@Test
	void getQtdNoTeste5() {
		arvore.inserir(3);
		arvore.inserir(2);
		arvore.inserir(5);
		arvore.inserir(1);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(6);
		Assertions.assertEquals(7, arvore.getQtdNo());
	}

	/**
	 * Testando metodo isEmpty, para retornar se a arvore � vazia
	 * 
	 */
	@Test
	void isEmptyTeste1() {

		Assertions.assertTrue(arvore.isEmpty());

	}

	/**
	 * Testando metodo isEmpty, para retornar se a arvore � vazia com elementos
	 * 
	 */
	@Test
	void isEmptyTeste2() {
		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		Assertions.assertFalse(arvore.isEmpty());

	}

	/**
	 * Testando metodo isFull, para retornar se a arvore degenerada e cheia
	 */
	@Test
	void isFullTeste1() {
		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		Assertions.assertFalse(arvore.isFull());
	}

	/**
	 * Testando metodo isFull, para retornar se a arvore e cheia
	 */
	@Test
	void isFullTeste2() {
		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);
		Assertions.assertFalse(arvore.isFull());
	}

	/**
	 * Testando metodo isFull, para retornar se a arvore e cheia
	 */
	@Test
	void isFullTeste3() {
		arvore.inserir(3);
		arvore.inserir(2);
		arvore.inserir(5);
		Assertions.assertTrue(arvore.isFull());
		arvore.inserir(1);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(6);
		Assertions.assertTrue(arvore.isFull());
	}

	/**
	 * Testando metodo isFull, se a arvore estiver vazia
	 */
	@Test
	void isFullTeste4() {
		Assertions.assertFalse(arvore.isFull());
	}

	/**
	 * Testando metodo preOrdem interativo com pilha, e um arvore
	 */
	@Test
	void preOrdemTeste1() {
		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);

		Assertions.assertEquals("8 3 1 6 4 7 10 14 13", arvore.preOrdem());
	}

	/**
	 * Testando metodo preOrdem interativo com pilha, em uma arvore vazia
	 */
	@Test
	void preOrdemTeste2() {

		Assertions.assertEquals("", arvore.preOrdem());
	}

	/**
	 * Testando metodo preOrdem interativo com pilha, em uma arvore degenerada
	 */
	@Test
	void preOrdemTeste3() {
		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		Assertions.assertEquals("1 2 3 4", arvore.preOrdem());
	}

	/**
	 * Testando metodo preOrdem interativo com pilha, em uma arvore cheia
	 */
	@Test
	void preOrdemTeste4() {
		arvore.inserir(3);
		arvore.inserir(2);
		arvore.inserir(5);
		arvore.inserir(1);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(6);
		Assertions.assertEquals("3 2 1 3 5 4 6", arvore.preOrdem());
	}

	/**
	 * Testando metodo inOrdem interativo com pilha, em uma arvore degenerada
	 */
	@Test
	void inOrdemTeste1() {
		arvore.inserir(1);
		arvore.inserir(2);
		arvore.inserir(3);
		arvore.inserir(4);
		Assertions.assertEquals("1 2 3 4", arvore.inOrdem());
	}

	/**
	 * Testando metodo inOrdem interativo com pilha, em uma arvore
	 */
	@Test
	void inOrdemTeste2() {
		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);
		Assertions.assertEquals("1 3 4 6 7 8 10 13 14", arvore.inOrdem());
	}

	/**
	 * Testando metodo inOrdem interativo com pilha, em uma arvore vazia
	 */
	@Test
	void inOrdemTeste3() {
		Assertions.assertEquals("", arvore.inOrdem());
	}

	/**
	 * Testando metodo inOrdem interativo com pilha, em uma arvore cheia
	 */
	@Test
	void inOrdemTeste4() {
		arvore.inserir(3);
		arvore.inserir(2);
		arvore.inserir(5);
		arvore.inserir(1);
		arvore.inserir(3);
		arvore.inserir(4);
		arvore.inserir(6);
		Assertions.assertEquals("1 2 3 3 4 5 6", arvore.inOrdem());
	}

	/**
	 * Testando metodo percorrer, verificando valores
	 */
	@Test
	void percorrerTeste1() {
		arvore.inserir(8);
		arvore.inserir(3);
		arvore.inserir(10);
		arvore.inserir(1);
		arvore.inserir(6);
		arvore.inserir(14);
		arvore.inserir(4);
		arvore.inserir(7);
		arvore.inserir(13);

		Assertions.assertEquals("8 3 1 6 4 7 10 14 13", arvore.percorrer());
		Assertions.assertEquals("8 3 1 6 4 7 10 14 13", arvore.percorrer());
	}

	/**
	 * Testando metodo percorrer, verificando valores de uma lista vazia
	 */
	@Test
	void percorrerTeste2() {
		Assertions.assertEquals("", arvore.percorrer());
	}

}
